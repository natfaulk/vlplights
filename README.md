# Compiling / Flashing

## Dependencies
- [ESP8266 core for Arduino](https://github.com/esp8266/Arduino)
- [arduinoFFT](https://github.com/kosme/arduinoFFT)
- [ArduinoJson](https://github.com/bblanchon/ArduinoJson)

The above dependencies need to be downloaded and installed.

The board needs to be put into bootloader mode by using the following process:  
- Press and hold the flash button  
- Press and release the reset button  
- Release the flash button  

The board can then be flashed.

# Configuration
On boot, each device attempts to connect to a network named `HiddenNetwork` with a password of `hiddenpassword`. Currently it will hang until it is able to connect (this may be changed in the future). Once connected it will print its IP address on the serial port. It will also attempt to post its current IP address, Hardware ID, and hostname to the central webapp server.

## Web app
There is a web app to simplify the configuration of the devices. For this to work, the devices need to be able to connect to the internet on boot to update their local ip. To configure a device the device which has loaded the webapp needs to be on the same network as the device being configured. This is because configuration does not go through the server but direct to the devices.

The webapp is located at [vlp.natfaulk.com](http://vlp.natfaulk.com/) and the code for the webapp is [here](https://bitbucket.org/natfaulk/vlplightsserver/src/master/)

## Without the web app
It is possible to configure the devices without the web app. This can be done by directly posting to the device's IP address with the API shown below.

### Enable/disable oscillator
**Request type:** POST  
**URL:** /setenabled  
**Arguments:** ledA, ledB, ledC  
**Values:** "on", "off"  
**Return type:** text/plain  
**Return value:** "ack"  
E.g. ledA=on&ledB=off

### Set oscillator frequency
**Request type:** POST  
**URL:** /setfreq  
**Arguments:** ledA, ledB, ledC  
**Values** Integer frequency  
**Return type:** text/plain  
**Return value:** "ack"  
E.g. ledA=3000

### Set bias resistance
**Request type:** POST  
**URL:** /setbiasres  
**Arguments:** res  
**Values:** Integer resistance  
**Return type:** text/plain  
**Return value:** "ack"  
E.g. res=3000

### Set bias resistance
**Request type:** POST  
**URL:** /setsigres  
**Arguments:** res  
**Values:** Integer resistance  
**Return type:** text/plain  
**Return value:** "ack"  
E.g. res=3000

### Get FFT
**Request type:** GET  
**URL:** /getfft  
**Return type:** application/json  
**Return value:** list off FFT values  

### Get current settings
**Request type:** GET  
**URL:** /getsettings  
**Return type:** application/json  
**Return value:** JSON object with settings  

### Get current FW version
**Request type:** GET  
**URL:** /getversion  
**Return type:** text/plain  
**Return value:** FW version string  

### Example:
To set frequency of device at `192.168.88.211` on oscillator A to 3100 and oscillator B to 2800 using curl:  
```
curl -X POST \
  http://192.168.88.211/setfreq \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -d 'ledA=3100&ledB=2800'
```

One can also use a GUI program such as [https://www.getpostman.com/apps](postman)