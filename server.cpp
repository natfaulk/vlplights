#include "server.hpp"
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include "fft.hpp"
#include "version.hpp"
#include <ArduinoJson.h>

#define SERVER_DEBUG_ENABLE 1

#if SERVER_DEBUG_ENABLE
#include "debug.hpp"
#endif

static ESP8266WebServer server(80);


void rootHandler(void);
void notfoundHandler(void);
void setfreqHandler(void);
void setwiperHandler(void);
void setenabledHandler(void);
void setbiasresHandler(void);
void setsigresHandler(void);
void getfftHandler(void);
void getsettingsHandler(void);
void getversionHandler(void);

enum {LEDA, LEDB, LEDC};
LedFrequency* leds[3];

void SERVER_Init(LedFrequency* _ledA, LedFrequency* _ledB, LedFrequency* _ledC)
{
  leds[LEDA] = _ledA;
  leds[LEDB] = _ledB;
  leds[LEDC] = _ledC;
  server.on("/", rootHandler);
  server.on("/setfreq", setfreqHandler);
  server.on("/setwiper", setwiperHandler);
  server.on("/setenabled", setenabledHandler);
  server.on("/setbiasres", setbiasresHandler);
  server.on("/setsigres", setsigresHandler);
  server.on("/getfft", getfftHandler);
  server.on("/getsettings", getsettingsHandler);
  server.on("/getversion", getversionHandler);
  server.onNotFound(notfoundHandler);
  server.begin();
}

bool SERVER_sendIp(const char* _hostname)
{
  bool returnVal = false;
  HTTPClient http;
  http.begin("http://vlp.natfaulk.com/set_ip");
  http.addHeader("Content-Type", "application/x-www-form-urlencoded");
  String ip = String("ip=");
  ip += WiFi.localIP().toString();
  ip += "&hostname=";
  ip += _hostname;
  ip += "&hwid=";
  ip += ESP.getChipId();
  
  int httpCode = http.POST(ip);
  if(httpCode > 0) {
    if(httpCode == HTTP_CODE_OK) {
      String payload = http.getString();
      if (payload == "ack") returnVal = true;
    }
  }
  http.end();
  return returnVal;
}

void SERVER_Tick(void)
{
  server.handleClient();
}

void rootHandler(void)
{
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.sendHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS");
  server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  server.send(200, "text/plain", "Hello world!");
#if SERVER_DEBUG_ENABLE
  DBG_pair("Served / to", server.client().remoteIP().toString().c_str());
#endif
}

void notfoundHandler(void)
{
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.sendHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS");
  server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  server.send(404, "text/plain", "404: Not found");
#if SERVER_DEBUG_ENABLE
  DBG_pair("Served 404 to", server.client().remoteIP().toString().c_str());
#endif
}

void setfreqHandler(void)
{
  String f1 = server.arg("ledA");
  String f2 = server.arg("ledB");
  String f3 = server.arg("ledC");

#if SERVER_DEBUG_ENABLE
  String temp = "Received new frequencies from " + server.client().remoteIP().toString();
  DBG_s(temp.c_str());
  temp = "f1 = "  + f1 + ", f2 = " + f2 + ", f3 = " + f3;
  DBG_s(temp.c_str());  
#endif

  if (f1 != "") leds[LEDA]->setFrequency(f1.toInt());
  if (f2 != "") leds[LEDB]->setFrequency(f2.toInt());
  if (f3 != "") leds[LEDC]->setFrequency(f3.toInt());

  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.sendHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS");
  server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  server.send(200, "text/plain", "ack");
}

void setwiperHandler(void)
{
  String f1 = server.arg("ledA");
  String f2 = server.arg("ledB");
  String f3 = server.arg("ledC");

#if SERVER_DEBUG_ENABLE
  String temp = "Received new wiper values from " + server.client().remoteIP().toString();
  DBG_s(temp.c_str());
  temp = "w1 = "  + f1 + ", w2 = " + f2 + ", w3 = " + f3;
  DBG_s(temp.c_str());  
#endif

  if (f1 != "") leds[LEDA]->setWiper(f1.toInt());
  if (f2 != "") leds[LEDB]->setWiper(f2.toInt());
  if (f3 != "") leds[LEDC]->setWiper(f3.toInt());

  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.sendHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS");
  server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  server.send(200, "text/plain", "ack");
}

void setenabledHandler(void)
{
  String f1 = server.arg("ledA");
  String f2 = server.arg("ledB");
  String f3 = server.arg("ledC");

  if (f1 == "on") leds[LEDA]->enable();
  if (f2 == "on") leds[LEDB]->enable();
  if (f3 == "on") leds[LEDC]->enable();

  if (f1 == "off") leds[LEDA]->disable();
  if (f2 == "off") leds[LEDB]->disable();
  if (f3 == "off") leds[LEDC]->disable();

  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.sendHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS");
  server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  server.send(200, "text/plain", "ack");

#if SERVER_DEBUG_ENABLE
  String temp = "Received enabled/disabled from " + server.client().remoteIP().toString();
  DBG_s(temp.c_str());
  
  temp = "f1: ";
  if (f1 == "on") temp += "Enabled";
  else if (f1 == "off") temp += "Disabled";
  else temp += "No change";

  temp += ", f2: ";
  if (f2 == "on") temp += "Enabled";
  else if (f2 == "off") temp += "Disabled";
  else temp += "No change";

  temp += ", f3: ";
  if (f3 == "on") temp += "Enabled";
  else if (f3 == "off") temp += "Disabled";
  else temp += "No change";
  DBG_s(temp.c_str());  
#endif
}

void setbiasresHandler(void)
{
  String res = server.arg("res");

  if (res != "") setBiasLevel(res.toInt());

  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.sendHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS");
  server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  server.send(200, "text/plain", "ack");
#if SERVER_DEBUG_ENABLE
  String temp = "Received new bias resistance from " + server.client().remoteIP().toString();
  DBG_s(temp.c_str());
  DBG_pair("New resistance", res.toInt());
#endif
}

void setsigresHandler(void)
{
  String res = server.arg("res");

  if (res != "") setSignalLevel(res.toInt());

  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.sendHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS");
  server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  server.send(200, "text/plain", "ack");
#if SERVER_DEBUG_ENABLE
  String temp = "Received new signal resistance from " + server.client().remoteIP().toString();
  DBG_s(temp.c_str());
  DBG_pair("New resistance", res.toInt());
#endif
}

void getfftHandler(void)
{
#if SERVER_DEBUG_ENABLE  
  String temp = "FFT request from " + server.client().remoteIP().toString();
  DBG_s(temp.c_str());
  DBG_s("Calculating FFT");
#endif  
  String out = "[";
  updateFFT();
  double* fft_pointer = getLatestFFT();

#if SERVER_DEBUG_ENABLE  
  DBG_s("Formatting FFT data to be sent");
#endif

  for (uint16_t i = 0; i < NUM_SAMPLES; i++)
  {
    out += (*(fft_pointer+i));
    if (i < NUM_SAMPLES - 1) out += ",";
    else out += "]";
  }
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.sendHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS");
  server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  server.send(200, "application/json", out);

#if SERVER_DEBUG_ENABLE
  DBG_s("FFT data sent to client");
#endif  
}

void getsettingsHandler(void)
{
  String out;
  StaticJsonBuffer<500> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  root["biasr"] = getBiasLevel();
  root["signalr"] = getSignalLevel();
  root["fw_version"] = VERSION_STRING;

  JsonObject& ledA = root.createNestedObject("ledA");
  if (leds[LEDA]->getEnabled()) ledA["state"] = "on";
  else ledA["state"] = "off";
  ledA["freq"] = leds[LEDA]->getFrequency();
  ledA["wiper"] = leds[LEDA]->getWiper();

  JsonObject& ledB = root.createNestedObject("ledB");
  if (leds[LEDB]->getEnabled()) ledB["state"] = "on";
  else ledB["state"] = "off";
  ledB["freq"] = leds[LEDB]->getFrequency();
  ledB["wiper"] = leds[LEDB]->getWiper();

  JsonObject& ledC = root.createNestedObject("ledC");
  if (leds[LEDC]->getEnabled()) ledC["state"] = "on";
  else ledC["state"] = "off";
  ledC["freq"] = leds[LEDC]->getFrequency();
  ledC["wiper"] = leds[LEDC]->getWiper();

  root.printTo(out);

  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.sendHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS");
  server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  server.send(200, "application/json", out);

#if SERVER_DEBUG_ENABLE
  String temp = "Sent settings to " + server.client().remoteIP().toString();
  DBG_s(temp.c_str());
#endif
}

void getversionHandler(void)
{
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.sendHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS");
  server.sendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  server.send(200, "text/plain", VERSION_STRING);
#if SERVER_DEBUG_ENABLE
  String temp = "Sent version number to " + server.client().remoteIP().toString();
  DBG_s(temp.c_str());
#endif
}