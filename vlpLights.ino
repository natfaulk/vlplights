#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <Wire.h>
#include "arduinoFFT.h"

#include "debug.hpp"
#include "ledFreq.hpp"
#include "fft.hpp"
#include "server.hpp"

// const uint32_t  BAUD_RATE                               = 460800;
const uint32_t  BAUD_RATE                               = 115200;
const char*     SSID                                    = "HiddenNetwork";
const char*     PASSWORD                                = "hiddenpassword";
const int       BOARD_ID                                = 11;

LedFrequency ledA(5, POT1, WIPER_0, WIPER_1);
LedFrequency ledB(4, POT1, WIPER_2, WIPER_3);
LedFrequency ledC(16, POT2, WIPER_2, WIPER_3);

void setup(void)
{
  DBG_Setup(BAUD_RATE);
  DBG_s("Setup started");

  char hostname[20];
  snprintf(hostname, 20, "VLP%d", BOARD_ID);
  WiFi.hostname(hostname);
  DBG_pair("Hostname set to ", hostname);

  WiFi.begin(SSID, PASSWORD);
  DBG_s("Connecting to ", false);DBG_s(SSID);
  
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    DBG_s(".", false);
  }
  DBG_nl();

  DBG_s("Connection successful");  
  DBG_pair("IP Address", WiFi.localIP().toString().c_str());

  if (MDNS.begin(hostname))
  {
    MDNS.addService("http", "tcp", 80);
    DBG_s("MDNS started");
  }
  else DBG_s("MDNS failed to start");

  Wire.begin(2, 14);
  DBG_s("I2C initialised");
  delay(100);

  FFT_Init();
  DBG_s("FFT initialised");

  ledA.init();
  ledB.init();
  ledC.init();
  DBG_s("Oscillators initialised");  

  setBiasLevel(5000);
  // setSignalLevel(5000);
  setSignalLevel(200);
  DBG_s("Signal levels initialised");

  ledA.enable();
  ledB.enable();
  ledC.enable();

  ledA.setFrequency(2700);
  ledB.setFrequency(3000);
  ledC.setFrequency(3300);
  DBG_s("Default oscillator settings loaded");

  DBG_s("Sending IP address to central server");
  if (SERVER_sendIp(hostname)) DBG_s("IP address successfully sent");
  else DBG_s("IP address failed to send");
  
  SERVER_Init(&ledA, &ledB, &ledC);
  DBG_s("HTTP server started");

  DBG_s("Setup finished");
}

void loop(void)
{
  SERVER_Tick();
  // FFT_Tick();
}