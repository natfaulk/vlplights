#include "ledFreq.hpp"
#include <Arduino.h>
#include <Wire.h>
#include <vector>

#include "debug.hpp"
#include "fft.hpp"

static inline double getResistFromFreq(uint32_t _freq);
static inline uint16_t getWiperFromResist(double _resist);

// need to be able to disable leds for fft feedback
// then need to restor previously turned on ones
void disableAllLeds(void);
void restoreAllLeds(void);

static uint32_t biasLevel = 5000;
static uint32_t signalLevel = 5000;

// in case starts oscillates and never settles on a value.
// the fft pid loop will exit after 20 loops.
const uint32_t MAX_PID_LOOPS = 20;

// to disable leds need to be able to access all leds from individual led
// member function. Therefore need  to store a list of all initialised leds
// Leds added in constructor
// Theoretically should have a destructor that removes leds from the led vector
// however it is assumed that leds are created and not destroyed ever as usually only need three
// If they (the led objects, not the pointers) were ever added to a vecotr or something this could cause problems
static std::vector<LedFrequency *> leds;
static std::vector<bool> stateBackup;

LedFrequency::LedFrequency(uint16_t _pin, uint8_t _i2cAddr, uint8_t _wiper1Addr, uint8_t _wiper2Addr)
  : mPin(_pin)
  , mI2cAddr(_i2cAddr)
  , mWiper1Addr(_wiper1Addr)
  , mWiper2Addr(_wiper2Addr)
  , mDesired(3000)
  , mCurrent(3000)
  , mEnable(false)
{
  init();
}

void LedFrequency::init(void)
{
  pinMode(mPin, OUTPUT);
  digitalWrite(mPin, mEnable);
  leds.push_back(this);
  stateBackup.push_back(mEnable);
}

void LedFrequency::enable(void)
{
  if (!mEnable)
  {
    mEnable = true;
    digitalWrite(mPin, 1);
  }
}

void LedFrequency::disable(void)
{
  if (mEnable)
  {
    mEnable = false;
    digitalWrite(mPin, 0);
  }
}

bool LedFrequency::getEnabled(void)
{
  return mEnable;
}

void LedFrequency::writeResistance(uint16_t _res)
{
  mWiper = _res;
  Wire.beginTransmission(mI2cAddr);
  Wire.write(mWiper1Addr);
  Wire.write(byte(_res));
  Wire.write(mWiper2Addr);
  Wire.write(byte(_res));
  Wire.endTransmission();
}


void LedFrequency::setFrequency(uint32_t _freq)
{
  mCurrent = _freq;
  uint8_t prevRes = 0;
  double resistance = getResistFromFreq(_freq);
  uint8_t wiperLevel = getWiperFromResist(resistance);

  writeResistance(wiperLevel);

  DBG_s("Starting FFT PID feedback");
  // need to make signal level lower so that ADC isnt saturated
  uint32_t previousSignalLevel = signalLevel;
  setSignalLevel(200);

  // disable all other frequencies so can sort out the one we intersted in
  disableAllLeds();
  this->enable();

  double difference = double(_freq) - FFT_getMajorPeak();
  uint32_t count = 0;
  while (prevRes != uint8_t(wiperLevel) && count < MAX_PID_LOOPS)
  {
    count++;
    DBG_pair("prevRes", prevRes);
    DBG_pair("wiperLevel", wiperLevel);
    prevRes = wiperLevel;
    difference = double(_freq) - FFT_getMajorPeak();
    DBG_pair("FFT difference", difference);
    wiperLevel += 0.05 * difference;
    writeResistance(wiperLevel);
    yield();
  }
  
  this->disable(); // in case adjusting frequency of truned off oscillator
  restoreAllLeds();
  // set back to previous signal level
  setSignalLevel(previousSignalLevel);
}

uint32_t LedFrequency::getFrequency(void)
{
  return mCurrent;
}

void LedFrequency::setWiper(uint32_t _wiper)
{
  writeResistance(_wiper);
}

uint32_t LedFrequency::getWiper(void)
{
  return mWiper;
}

void LedFrequency::getFFT(void)
{

}

void disableAllLeds(void)
{
  for (uint8_t i = 0; i < leds.size(); i++)
  {
    stateBackup.at(i) = leds.at(i)->getEnabled();
    leds.at(i)->disable();
  }
}

void restoreAllLeds(void)
{
  for (uint8_t i = 0; i < leds.size(); i++)
  {
    if(stateBackup.at(i)) leds.at(i)->enable();
  }
}

// calculates resistance value from frequency
double getResistFromFreq(uint32_t _freq)
{
  return (10000 - (100000000 / (_freq * 2 * 3.1415)));
}

// gets wiper level on digital pot for resistance
uint16_t getWiperFromResist(double _resist)
{
  return round(((_resist - 75) * 256.0) / 10000.0);
}

void setBiasLevel(uint32_t _resistance)
{ 
  biasLevel = _resistance;
  Wire.beginTransmission(POT2);
  Wire.write(WIPER_1);
  Wire.write(uint8_t(getWiperFromResist(_resistance)));
  Wire.endTransmission();
}

uint32_t getBiasLevel(void)
{
  return biasLevel;
}

void setSignalLevel(uint32_t _resistance)
{
  signalLevel = _resistance;
  Wire.beginTransmission(POT2);
  Wire.write(WIPER_0);
  Wire.write(uint8_t(getWiperFromResist(_resistance)));
  Wire.endTransmission();
}

uint32_t getSignalLevel(void)
{
  return signalLevel;
}