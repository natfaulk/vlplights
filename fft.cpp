#include <Arduino.h>
#include "arduinoFFT.h"
#include "debug.hpp"
// #include "ledFreq.hpp"
#include "fft.hpp"

void timer_ISR(void);
void printData(void);

// const uint32_t BAUD_RATE = 460800;

volatile bool dataFull = false;
volatile uint16_t data[NUM_SAMPLES];
volatile uint16_t dataI = 0;

unsigned long prevTime = 0;

double vReal[NUM_SAMPLES];
double vImag[NUM_SAMPLES];
arduinoFFT FFT = arduinoFFT(vReal, vImag, NUM_SAMPLES, SAMPLE_RATE);

bool t = false;

void FFT_Init(void)
{
  timer1_isr_init();
  timer1_attachInterrupt(timer_ISR);
  timer1_enable(TIM_DIV1, TIM_EDGE, TIM_LOOP);
  timer1_write(80000000 / SAMPLE_RATE);
}

// void FFT_Tick(void)
// {
//   if (dataFull && (millis() - prevTime) > UPDATE_RATE_ms)
//   {
//     for (int i = 0; i < NUM_SAMPLES; i++)
//     {
//       vReal[i] = data[i];
//       vImag[i] = 0;
//     }

//     dataFull = false;


      
//     FFT.Windowing(vReal, NUM_SAMPLES, FFT_WIN_TYP_HAMMING, FFT_FORWARD);  /* Weigh data */
//     FFT.Compute(vReal, vImag, NUM_SAMPLES, FFT_FORWARD); /* Compute FFT */
//     FFT.ComplexToMagnitude(vReal, vImag, NUM_SAMPLES); /* Compute magnitudes */

//     printData();
//     prevTime = millis();
//   }
// }

double FFT_getMajorPeak(void)
{
  updateFFT();
  return FFT.MajorPeak();
}

void updateFFT(void)
{
  dataFull = false;
  dataI = 0;

  // hang until fft done
  // better to make it polled
  // TODO: get rid of this or at least add timeout
  while (!dataFull);

  for (int i = 0; i < NUM_SAMPLES; i++)
  {
    vReal[i] = data[i];
    vImag[i] = 0;
  }

  FFT.Windowing(FFT_WIN_TYP_HAMMING, FFT_FORWARD);  /* Weigh data */
  FFT.Compute(FFT_FORWARD); /* Compute FFT */
  FFT.ComplexToMagnitude(); /* Compute magnitudes */
}

double* getLatestFFT(void)
{
  return vReal;
}

void timer_ISR(void)
{
  if (!dataFull)
  {
    data[dataI++] = analogRead(ANALOG_PIN);
    if (dataI >= NUM_SAMPLES) 
    {
      dataFull = true;
      dataI = 0;
    }
  }
}

void printData(void)
{
  DBG_s("ADC Data: ", false);
  DBG_s("[", false);
  for (uint16_t i = 0; i < NUM_SAMPLES; i++)
  {
    DBG_f(vReal[i], false);
    if (i < NUM_SAMPLES - 1) DBG_s(",", false);
    yield();
  }
  DBG_s("]");
}
