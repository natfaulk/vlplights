#ifndef SERVER_HPP
#define SERVER_HPP

#include "ledFreq.hpp"

void SERVER_Init(LedFrequency*, LedFrequency*, LedFrequency*);
void SERVER_Tick(void);
bool SERVER_sendIp(const char* _hostname);

#endif