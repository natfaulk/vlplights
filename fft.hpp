#ifndef FFT_HPP
#define FFT_HPP

static const uint8_t ANALOG_PIN = A0;
static const uint16_t SAMPLE_RATE = 8000;
static const uint16_t NUM_SAMPLES = 1024;
static const uint16_t UPDATE_RATE_ms = 250;

void FFT_Init(void);
// void FFT_Tick(void);
void updateFFT(void);
double FFT_getMajorPeak(void);
double* getLatestFFT(void);

#endif
