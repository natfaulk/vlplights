#ifndef LED_FREQ_HPP
#define LED_FREQ_HPP

#include <stdint.h>

const uint8_t   POT1                                    = 0x2e;
const uint8_t   POT2                                    = 0x2c;

const uint8_t   WIPER_0                                 = 0x00;
const uint8_t   WIPER_1                                 = 0x10;
const uint8_t   WIPER_2                                 = 0x60;
const uint8_t   WIPER_3                                 = 0x70;

class LedFrequency
{
public:
  LedFrequency(uint16_t _pin, uint8_t _i2cAddr, uint8_t _wiper1Addr, uint8_t _wiper2Addr);
  void init(void);
  void getFFT(void);
  void setFrequency(uint32_t _freq);
  void setWiper(uint32_t _wiper);
  uint32_t getFrequency(void);
  uint32_t getWiper(void);
  void enable(void);
  void disable(void);
  bool getEnabled(void);
private:
  void writeResistance(uint16_t _res);
  uint16_t mPin;
  uint8_t mI2cAddr;
  uint8_t mWiper1Addr;
  uint8_t mWiper2Addr;
  uint32_t mDesired;
  uint32_t mCurrent;
  uint32_t mWiper;
  bool mEnable;
};

void setBiasLevel(uint32_t _resistance);
void setSignalLevel(uint32_t _resistance);
uint32_t getBiasLevel(void);
uint32_t getSignalLevel(void);

#endif